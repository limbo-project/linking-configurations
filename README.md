# About Linking in LIMBO

* Potential linking candidates for each data set are in [our Confluence](https://confluence.brox.de/display/LIMBO/Raw+Data+-+Mapping+Status)
* For each linking candidate pair, we need to provide a configuration
* The *linking service* will fetch all the configurations from the link repositories, execute them and add the results back to the corresponding link repository
* We use the following placeholders inside our configuration XML:
  * `%source%` - the identifier of the source 
  * `%sourceFile%` - the name of the sourceFile
  * `%target%` - the identifier of the target
  * `%targetFile%` - the name of the targetFile
* These placeholders will be replaced automatically by the linking service.
* However, in cases where we want to interlink with external datasets which we do not include in LIMBO, e.g. DBpedia, `targetFile` should not be used. Instead, insert the appropriate SPARQL endpoint URI. (And change the type to `<TYPE>SPARQL</TYPE>`)

## Todo 

* [ ]  Wait for datasets to become available in gitlab
* [ ] Assemble and test appropriate linking configuration for the pairs indicated in [our Confluence](https://confluence.brox.de/display/LIMBO/Raw+Data+-+Mapping+Status)
* [ ] Upload configurations here (naming scheme: `${sourceName}-TO-${targetName}.xml`)
* [ ] When first configuration is ready, contact Kevin to test linking service.